# iftns

Information Field Theory for Nested Sampling evidence calculation


Nested sampling (NS) is a stochastic method for computing the log-evidence of a Bayesian problem.
It relies on stochastic estimates of the prior volume enclosed by the likelihood contours, which limits
the accuracy of the log-evidence calculation.  We developed an algorithm that transforms the prior volume 
estimation into a Bayesian inference problem, which allows us to incorporate a smoothness assumption for 
likelihood-prior volume relations. As a result, we aim to increase the accuracy of the volume estimates 
and thus improve the overall log-evidence calculation using NS. The implemented algoithm works as a post-processing
step for NS and provides posterior samples of the likelihood-prior-volume relation, from which the log-evidence can be 
calculated. 

## Requirements
- [NIFTy8](https://gitlab.mpcdf.mpg.de/ift/nifty) 
- [anesthetic](https://github.com/handley-lab/anesthetic)
- JAX
- matplotlib


## Installation of Dependencies
- Information on how to install NIFTy8 can be found [here](https://gitlab.mpcdf.mpg.de/ift/nifty)
- Information on how to install anesthetic can be found [here](https://github.com/handley-lab/anesthetic)

## Installation
This package can be installed via pip. 

    git clone https://gitlab.mpcdf.mpg.de/ift/students/margret/iftns
    cd iftns
    pip install --user .

for a regular installation. For editable installation add the `-e` flag.

## Notes

### Docstring Contributions

A small fraction of the docstrings in this project have been generated using GitHub Copilot.