import argparse
import numpy as np
import os
import matplotlib.pyplot as plt

import iftns as ins
import nifty8 as ift

def main():
    # get config from terminal config file name
    parser = argparse.ArgumentParser()
    parser.add_argument("config_file", type=str)
    args = parser.parse_args()
    cfg = ins.yaml_to_dict(args.config_file)

    # get dictionaries from config
    signal_setup = cfg['signal_setup']
    prior_info = cfg['priors_tau']
    opt_dict = cfg['optimization']

    # set random seed for reconstruction
    ift.random.push_sseq_from_seed(int(cfg['seed']))

    # create output dictionary and save config file there
    os.makedirs(cfg['results_dir'], exist_ok=True)
    ins.save_dict_to_yaml(cfg, os.path.join(cfg['results_dir'], 'rec_config.yaml'))

    # initiate MPI
    try:
        from mpi4py import MPI
        comm = MPI.COMM_WORLD
        master = comm.Get_rank() == 0.
    except ImportError:
        comm = None
        master = True

    # Load data
    data_dict = {}
    x_data, lnL_data, n_live, samples = ins.load_ns_data(cfg['data_path'])
    data_config = ins.yaml_to_dict(os.path.join(cfg['data_path'], 'data_config.yaml'))

    # reparametrise data lnL to f(lnL)
    if 'lnL_max' not in signal_setup:
        lnL_max = ins.get_lnL_max(x_data, lnL_data, data_config['dim'])
        signal_setup['lnL_max'] = lnL_max
        ins.update_config(cfg, signal_setup, 'signal_setup',
                          os.path.join(cfg['results_dir'], f'rec_config.yaml'),
                          os.path.join(cfg['results_dir'], f'rec_config_old_lnL_max.yaml'),
                          master)
    else:
        lnL_max = signal_setup['lnL_max']
    flnL_data = ins.reparametrize_lnL(lnL_data, lnL_max)
    minus_lnX = -np.log(x_data)

    # initiate priors for the reparametrised likelihood prior volume function f(lnL) and the prior volumes lnX
    func_info = ins.FuncPrior(signal_setup, np.max(minus_lnX)-np.min(minus_lnX))
    lnX_coord_depadded = np.linspace(0, func_info.depadded_domain.size *
                                   func_info.depadded_domain.distances[0],
                                   func_info.depadded_domain.size)
    flnL0 = ins.get_flnL0(data_config, flnL_data)
    gt_dict = ins.get_ground_truth_contours(data_config, lnX_coord_depadded, func_info, flnL0,
                                            lnL_max)

    # get data dictionary
    data_dict['x'] = x_data
    data_dict[func_info.flnL_key] = flnL_data
    data_dict[func_info.l_key] = np.exp(lnL_data)

    # Calculate offset mean of correlated field analytically if not set in config
    if 'offset_mean' not in prior_info:

        tau_offset_mean = float(round(ins.get_offset_mean(data_config,
                                                          flnL_data,
                                                          x_data,
                                                          gt_dict[func_info.flnL_key],
                                                          np.exp(-lnX_coord_depadded)), 1))
        prior_info['offset_mean'] = tau_offset_mean
        ins.update_config(cfg, prior_info, 'priors_tau',
                          os.path.join(cfg['results_dir'], f'rec_config.yaml'),
                          os.path.join(cfg['results_dir'], f'rec_config_old_offset_mean.yaml'),
                          master)

    comm.Barrier()

    # get generative prior operators for reparametrised likelihood prior volume function
    func_info.create_flnL_prior_volume_function(flnL0, prior_info)
    func_info.create_l_prior_volume_function(flnL0, lnL_max=lnL_max)

    # get generativr prior operators for the prior volumes
    if n_live.shape == ():
        n_live = int(n_live)
    elif len(n_live.shape) == 1 and n_live.shape[0] == x_data.shape[0]:
        n_live = ift.Field.from_raw(ift.UnstructuredDomain([1, x_data.shape[0]]),
                                    n_live.reshape(1, n_live.shape[0]))
    else:
        raise NotImplementedError
    lnX_info = ins.lnXPrior(n_live, x_data.shape[0])
    lnX_info = ins.lnXPrior(n_live, x_data.shape[0])
    lnX_info.create_t_op()
    lnX_info.create_lnX_op()

    # plot priors
    ins.plot_ns_prior_samples(func_info, -lnX_coord_depadded, 'lnX', gt_dict=gt_dict,
                              output_dir=cfg['results_dir'])

    # interpolate the likelihood prior volume function at inferred lnX points
    intpol = ins.Interpolation(func_info.domain, func_info.flnL_key, lnX_info.domain, lnX_info.lnX_key)
    flnL_vals = intpol @ (func_info.flnL_op + lnX_info.lnX_op)

    # find suiting initial position via MAP on nested sampling estimate if desired
    flnL_data_field = ift.Field.from_raw(lnX_info.reshaped_domain, flnL_data)
    if opt_dict['find_init']:
        intpol_lin = ift.LinearInterpolator(func_info.domain,
                                            np.reshape(minus_lnX,(1, minus_lnX.shape[0]))).\
                                            ducktape(func_info.flnL_key)
        intpol_chain = intpol_lin @ func_info.flnL_op
        sigma_delta_map = np.min(np.diff(flnL_data))/10
        init_measurement_noise = ift.ScalingOperator(flnL_vals.target,
                                                     sigma_delta_map ** 2,
                                                     np.float64)
        init_measurement_lklhd = ift.GaussianEnergy(flnL_data_field,
                                                    inverse_covariance=init_measurement_noise.inverse)\
                                 @ intpol_chain

        init_ic_newton = ift.AbsDeltaEnergyController(**cfg['init_minimizer'])
        init_minimizer = ift.NewtonCG(init_ic_newton)

        def init_callback(sl, i_global):
            fig_dir = os.path.join(cfg['results_dir'], 'init/', f'fig_{i_global}')
            os.makedirs(fig_dir, exist_ok=True)
            ins.plot_rec_func_from_sample_list(sl, func_info, -lnX_coord_depadded, 'lnX',
                                               data_dict, gt_dict, output_dir=fig_dir)
        _, init_mean = ift.optimize_kl(init_measurement_lklhd,
                                       cfg['init_optimization']['n_iterations'],
                                       0,
                                       init_minimizer,
                                       None,
                                       None,
                                       inspect_callback=init_callback,
                                       output_directory=os.path.join(cfg['results_dir'], 'init'),
                                       resume=True,
                                       return_final_position=True)

    # set minimizers for the joint inference of prior volumes and function
    ic_sampling = ift.AbsDeltaEnergyController(**cfg['ic_sampling'])
    ic_newton = ift.AbsDeltaEnergyController(**cfg['ic_newton'])
    ic_sampling_nl = ift.AbsDeltaEnergyController(**cfg['ic_sampling_nl'])
    minimizer = ift.NewtonCG(ic_newton)
    minimizer_sampling = ift.NewtonCG(ic_sampling_nl)

    # generate schedule for sigma delta and samples
    sigma_delta_list = np.logspace(np.log10(opt_dict['sigma_min_percentage'] *
                                            np.min(np.diff(flnL_data))),
                                   np.log10(np.max(np.diff(flnL_data))),
                                   opt_dict['num_schedule_steps'], base=10)
    sample_list = np.linspace(opt_dict['init_n_samples'], opt_dict['final_n_samples'],
                              opt_dict['num_schedule_steps']).astype(int)

    if not opt_dict['sigma_schedule']:
        sigma_delta_list = [sigma_delta_list.tolist()[0]]*len(sample_list)
    if not opt_dict['sample_schedule']:
        sample_list = [sample_list.tolist()[-1]]*len(sigma_delta_list)
    if not opt_dict['sigma_schedule'] and not opt_dict['sample_schedule']:
        sample_list = [sample_list.tolist()[-1]]
        sigma_delta_list = [sigma_delta_list.tolist()[0]]
    def rec_callback(sl, i_global):
        fig_dir = os.path.join(info_dir, f'fig_{i_global}')
        os.makedirs(fig_dir, exist_ok=True)
        ins.plot_rec_func_from_sample_list(sl, func_info, -lnX_coord_depadded, 'lnX',
                                           data_dict, gt_dict, output_dir=fig_dir)
        try:
            gt_lnL = np.log(gt_dict[func_info.l_key])
        except:
            gt_lnL = None
        try:
            ins.plot_prior_volumes_rec_results_from_sample_list(sl, lnX_info, samples,
                                                                data_config,
                                                                gt_lnL,
                                                                -lnX_coord_depadded,
                                                                fig_dir)
        except:
            ift.logger.info('Not able to plot hist')

    # Minimization of the KL using geoVI on given schedule
    for i, sigma_delta in enumerate(sigma_delta_list[::-1]):
        info_dir = os.path.join(cfg['results_dir'], f'rec_info_{sigma_delta}_{sample_list[i]}')
        gaussian_noise = ift.ScalingOperator(flnL_vals.target, sigma_delta**2, np.float64)
        lh = ift.GaussianEnergy(flnL_data_field, inverse_covariance=gaussian_noise.inverse) @ flnL_vals
        if i == 0:
            if opt_dict['find_init']:
                minus_X_ = np.roll(np.exp(-minus_lnX),1)
                minus_X_[0] = 1
                t_data = np.reshape(np.exp(-minus_lnX)/ minus_X_, lnX_info.lnX_op.domain['t_inp'].shape)
                beta_op = ins.BetaOperator(lnX_info.lnX_op.domain['t_inp'], n_live, 1)
                xi_t = beta_op.inverse(ift.Field(lnX_info.lnX_op.domain['t_inp'], t_data))
                xi_t_datafield = ift.MultiField.from_dict({'t_inp': xi_t})
                mean = init_mean.unite(xi_t_datafield)
            else:
                mean = ift.from_random(lh.domain)
        sl, mean = ift.optimize_kl(lh,
                                   opt_dict['n_iterations'],
                                   int(sample_list[i]),
                                   minimizer,
                                   ic_sampling,
                                   minimizer_sampling,
                                   output_directory=info_dir,
                                   return_final_position=True,
                                   initial_position=mean,
                                   comm=comm,
                                   inspect_callback=rec_callback,
                                   resume=True)



if __name__ == "__main__":
    main()



