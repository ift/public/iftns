import argparse
import numpy as np
import os
import matplotlib.pyplot as plt
import shutil

import iftns as ins
import nifty8 as ift

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("comb_folder", type=str)
    args = parser.parse_args()
    comb_folder_path = args.comb_folder

    if os.path.exists(os.path.join(comb_folder_path, 'combined')):
        shutil.rmtree(os.path.join(comb_folder_path, 'combined'))
    ins.check_configs(comb_folder_path)
    ins.check_rec_info_folders(comb_folder_path)
    combined_folder = ins.create_combined_folder(comb_folder_path)
    ins.combine_rec_info_sample_lists(comb_folder_path)

    cfg = ins.yaml_to_dict(os.path.join(combined_folder,
                                         'rec_config.yaml'))
    data_config = ins.yaml_to_dict(os.path.join(cfg['data_path'],
                                                'data_config.yaml'))

    x_data, lnL_data, n_live, samples = ins.load_ns_data(cfg['data_path'])
    minus_lnX = -np.log(x_data)
    func_info = ins.FuncPrior(cfg['signal_setup'],
                              np.max(minus_lnX) - np.min(minus_lnX))
    flnL0 = ins.get_flnL0(data_config)

    func_info.create_flnL_prior_volume_function(flnL0, cfg['priors_tau'])
    func_info.create_l_prior_volume_function(flnL0,
                                             lnL_max=cfg['signal_setup']['lnL_max'])

    lnX_coord_depadded = np.linspace(0, func_info.depadded_domain.size *
                                     func_info.depadded_domain.distances[0],
                                     func_info.depadded_domain.size)
    gt_dict = ins.get_ground_truth_contours(data_config, lnX_coord_depadded,
                                            func_info, flnL0, 0)

    flnL_data = ins.reparametrize_lnL(lnL_data, 0)
    data_dict = {}
    data_dict['x'] = x_data
    data_dict[func_info.flnL_key] = flnL_data
    data_dict[func_info.l_key] = np.exp(lnL_data)

    if n_live.shape == ():
        n_live = int(n_live)
    elif len(n_live.shape) == 1 and n_live.shape[0] == x_data.shape[0]:
        n_live = ift.Field.from_raw(ift.UnstructuredDomain([1, x_data.shape[0]]),
                                    n_live.reshape(1, n_live.shape[0]))
    else:
        raise NotImplementedError

    lnX_info = ins.lnXPrior(n_live, x_data.shape[0])
    lnX_info.create_t_op()
    lnX_info.create_lnX_op()

    ins.plot_comb_sl_results(comb_folder_path, func_info, lnX_info,
                             samples, -lnX_coord_depadded, r'$\ln X$',
                             data_config, data_dict, gt_dict)

if __name__ == "__main__":
    main()


