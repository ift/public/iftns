import os
import iftns as ins
def main():
    default_config_path = 'config_files/rec_config_default.yaml'
    data_config_path = 'config_files/data/data_config_summed_gauss_2_2.yaml'
    config_base = 'config_files/rec/'
    results_base= '../rec_results/'
    input_batch = 'batch/batch_default.sh'
    ntasks = 1
    list_of_seeds = [4, 7, 12, 16, 23, 30, 31, 42, 81]

    cfg = ins.yaml_to_dict(default_config_path)
    data_cfg = ins.yaml_to_dict(data_config_path)
    nlive = data_cfg['n_live_points']
    lklhd_shape = data_cfg['lklhd_shape']
    if input_batch is not None:
        with open(input_batch, 'r') as file:
            script_content = file.read()

    for seed in list_of_seeds:
        results_dir = os.path.join(results_base, f'new2_data_{lklhd_shape}_{nlive}_{seed}/')
        config_path = os.path.join(config_base, f'new2_data_config_{lklhd_shape}_{nlive}_{seed}.yaml')
        cfg['seed'] = seed
        cfg['data_path'] = data_cfg['output_dir']
        ins.update_config(cfg, results_dir, 'results_dir',
                          config_path)
        if input_batch is not None:
            batch_string = script_content
            batch_string += f'\n#SBATCH -n {ntasks}'
            batch_string += f'\nsrun python3 run_rec_on_ns_data.py {config_path}'
            batch_string += '\ncp tjob.${SLURM_JOB_ID}.err ' + f'{results_dir}' \
                          + '\ncp tjob.${SLURM_JOB_ID}.out ' + f'{results_dir}'
            batch_file = f'batch/new2_data_batch_{lklhd_shape}_{nlive}_{seed}.sh'
            with open(batch_file, 'w') as file:
                file.write(batch_string)
    print(config_path)
if __name__ == "__main__":
    main()