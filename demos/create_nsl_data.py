import argparse
import os

import nifty8 as ift
import iftns as ins

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("config_file", type=str)
    args = parser.parse_args()
    data_setup = ins.yaml_to_dict(args.config_file)

    ift.random.push_sseq_from_seed(int(data_setup['seed']))
    data_dir = data_setup['output_dir']

    ift.logger.info('Generating data')
    logLmin = data_setup.get('logLmin')
    if logLmin:
        ins.generate_ns_data(data_setup, data_dir, logLmin=logLmin)
    else:
        ins.generate_ns_data(data_setup, data_dir)

    ins.save_dict_to_yaml(data_setup, os.path.join(data_dir, 'data_config.yaml'))


if __name__ == "__main__":
    main()


