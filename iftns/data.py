# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# Copyright(C) 2013-2021 Max-Planck-Society

import numpy as np
from os.path import join, exists
from os import mkdir
import pickle

import iftns as ins

def generate_ns_data(setup, output_dir=None, n_samples=None, logLmin=-1e-2):

    """
    Generates nested sampling data and saves it to file

            Parameters:
                    setup (dict): Dictionary for likelihood setup
                    output_dir (str): Output folder
                    n_samples (int): Number of samples generated for statistical nested sampling
                                     approach. None for deterministic approach.
                    logLmin (float): Minimum loglikelihood
    """

    shape = setup['lklhd_shape']
    if shape == 'anesthetic_gauss':
        from anesthetic.examples.perfect_ns import gaussian
        n_live = np.array(setup['n_live_points'])
        samples = gaussian(int(n_live), setup['dim'], setup['sigma_1'], logLmin=logLmin)
    elif shape == 'anesthetic_summed_gauss':
        n_live = np.array(setup['n_live_points'])
        samples = ins.gaussian_sum(int(n_live), setup['dim'], setup['a'], m1 = 0, m2= 0,
                                sigma1=setup['sigma_1'],sigma2=setup['sigma_2'])
    elif shape == 'data':
        data_path = setup['data_path']
        from anesthetic import read_chains
        samples = read_chains(data_path)
    else:
        raise NotImplementedError
    x_data = np.exp(samples.logX(nsamples=n_samples)).to_numpy()
    y_data = samples.logL.to_numpy()
    n_live = samples.nlive.to_numpy()

    mkdir(output_dir)
    np.save(join(output_dir, 'x_data.npy'), x_data)
    np.save(join(output_dir, 'y_data.npy'), y_data)
    np.save(join(output_dir, 'n_live.npy'), n_live)
    samples.to_pickle(join(output_dir, 'samples.pkl'))


def load_ns_data(data_dir):

    """
    Generates nested sampling data and saves it to file

            Parameters:
                    data_dir (str): Directory form which to load the data
            Returns:
                    x_data (np.array): Array of prior volumes
                    y_data (np.array): Array of log-likelihoods
                    n_live (np.array): Array of number of livepoints
                    samples (class:'anesthetic.samples.NestedSamples'): Nested sampling run
    """
    y_data = np.load(join(data_dir, 'y_data.npy'))
    x_data = np.load(join(data_dir, 'x_data.npy'))
    with open(join(data_dir, 'samples.pkl'), 'rb') as file:
        samples = pickle.load(file)
    n_live = np.load(join(data_dir, 'n_live.npy'))
    return x_data, y_data, n_live, samples
