# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# Copyright(C) 2013-2021 Max-Planck-Society

import nifty8 as ift
import numpy as np
from scipy.stats import norm, beta

from .ns_functions import create_neglnX_from_contraction_factors, flnL_func
class BetaOperator(ift.Operator):
    """Class that transforms a standard normal into a beta distribution. The operator can be initialized by setting a and b.
    This transformation is implemented as a linear interpolation which maps a Gaussian onto a beta distribution.

            Parameters
                    domain (class:'ift.Domain' / class:'ift.DomainTuple' / tuple):
                            The domain on which the field shall be defined. This is at the same
                            time the domain and the target of the operator.
                    a (class:'ift.Field' / float): First shape parameter of the beta distirbution
                    b (class:'ift.Field' / float): Second shape parameter of the beta distribution
                    delta (float): Distance between sampling points for linear interpolation.
    """

    def __init__(self, domain, a, b, int_min=-8.2, int_max =8.2):
        self._domain = self._target = ift.DomainTuple.make(domain)
        if np.isscalar(a):
            self._a = ift.Field.full(self._domain, a)
        elif isinstance(a, ift.Field):
            if not a.domain == self._domain:
                raise ValueError
            self._a = a
        else:
            raise NotImplementedError
        if np.isscalar(b):
            self._b = ift.Field.full(self._domain, b)
        elif isinstance(b, ift.Field):
            if not b.domain == self._domain:
                raise ValueError
            self._b = b
        else:
            raise NotImplementedError
        self._mean = self._a / (self._a + self._b)
        self._delta = (int_max - int_min) / self._domain.shape[1]
        op = ift.library.special_distributions._InterpolationOperator(self._domain,
                                                                      lambda x: beta.ppf(norm._cdf(x),
                                                                                         self._a.val,
                                                                                         self._b.val)[0,:],
                                                                      int_min, int_max, self._delta)
        self._op = op

    def apply(self, x):
        return self._op(x)

    @property
    def a(self):
        """float : The value of the first shape parameter of the beta distribution"""
        return self._a

    @property
    def b(self):
        """float : The value of the second shape parameter of the beta distribution"""
        return self._b

    @property
    def mean(self):
        """float : The value of the mean of beta distribution. Only existing for alpha > 1."""
        return self._mean

    @property
    def var(self):
        """float : The value of the variance of the beta distribution."""
        if self._a + self._b <= -1:
            raise ValueError('variance only existing for alpha > 2')
        return self._a * self._b / ((self._a + self._b + 1) * (self._a + self._b) ** 2)

    def inverse(self, x):
        res = beta.cdf(x.val, self._a.val, self._b.val)
        res = norm._ppf(res)
        return ift.Field(x.domain, res)


class lnXPrior:
    """ Class that transform a standard normal into prior volumes, which are defined
    by beta-distributed contraction factors. This transformation is implemented as a linear interpolation
    which maps a Gaussian onto a beta distribution and is defined by the number of live-points at each iteration.

            Parameters
                    n_live (class:'ift.Field' / float): Number of live_points at each iteration
                    data_points (float): Number of data points
    """
    def __init__(self, n_live, data_points):
        self.reshaped_domain = ift.UnstructuredDomain(data_points)
        self.domain = ift.UnstructuredDomain([1, data_points])
        self.n_live = n_live
        self.z_key = None
        self.t_op = None
        self.z_op = None

    def create_t_op(self):
        """ Create beta-distributed contraction factors

                Returns
                        self.t_op (class:'ift.Operator'): Beta-distributed contraction factor operator

        """
        t_op = BetaOperator(self.domain, self.n_live, 1)
        self.t_op = t_op.ducktape('t_inp')
        return self.t_op

    def create_lnX_op(self, lnX_key='lnX_cords'):
        """ Create prior volumes from beta distributed contraction factors

                Returns
                        self.lnX_op (class:'ift.Operator'): Prior volumes rec-prior model operator
        """
        if self.t_op is None:
            _ = self.create_t_op()
        self.lnX_key = lnX_key
        self.lnX_op = create_neglnX_from_contraction_factors(self.t_op).ducktape_left(lnX_key)
        return self.lnX_op


class FuncPrior:
    """ Class that transform a standard normal into the prior for the likelihood-prior-volume-function
    and the reparametrized log-likelihood-prior-volume function

            Parameters
                    setup (dict): Dictionary for signal setup
                    data_range (float): Distance between the maximal and minimal data point for the prior volume
                    flnL_key (str): Key for reparametrized log-likelihood-prior-volume function
                    l_key (str): Key for log-likelihood-prior-volume function
    """
    def __init__(self, setup, data_range, flnL_key='flnL_tau', l_key='L_tau'):
        self.domain = ift.RGSpace(setup['padding_factor']*setup['npix'],
                                  distances=data_range/setup['npix'])
        self.depadded_domain = ift.RGSpace(setup['npix'],
                                           distances=data_range/ setup['npix'])
        self.padder = ift.FieldZeroPadder(self.depadded_domain,
                                          self.domain.shape)
        self.flnL_key = flnL_key
        self.l_key = l_key
        self.tau = None
        self.avail_ops = {}
        self.flnL_op = None
        self.l_op = None
    def _update_avail_ops(self, key, op):
        self.avail_ops[key] = op
    def create_correlated_field(self, priors_tau):
        """ Generation of the correlated field Operator describing the log-likelihood-prior-volume function
                    Parameters
                            priors_tau (dict): Prior hyperparameters for the correlated field model

                    Returns
                            tau (class:'ift.Operator'): Correlated field operator

        """
        tau = ift.SimpleCorrelatedField(self.domain, **priors_tau)
        self.tau = tau
        return tau

    def create_flnL_prior_volume_function(self, flnL0, priors_tau=None):
        """ Generation of the reparametrized log-likelihood prior volume function operator
                    Parameters
                            flnL0 (float): The initial flnL value
                            priors_tau (dict): Prior hyperparameters for the correlated field model
                    Returns
                            flnL_op (class:'ift.Operator'): Reparametrized log-likelihood prior
                                                            volume function operator

        """
        if self.tau is None:
            if priors_tau is None:
                raise ValueError("No prior knowledge.")
            _ = self.create_correlated_field(priors_tau)
        flnL_tau, flnL_tau_deriv = flnL_func(self.tau, flnL0)
        flnL_op = flnL_tau.ducktape_left(self.flnL_key)
        self.flnL_op = flnL_op
        self._update_avail_ops(self.flnL_key, self.flnL_op)
        return flnL_op

    def create_l_prior_volume_function(self, flnL0, lnL_max, priors_tau=None):
        """ Generation of the likelihood prior volume function operator
                    Parameters
                            flnL0 (float): The initial flnL value
                            lnL_max (float): The maximum log-likelihood
                            priors_tau (dict): Prior hyperparameters for the correlated field model
                    Returns
                            flnL_op (class:'ift.Operator'): Reparametrized log-likelihood prior
                                                            volume function operator

        """
        if self.tau is None:
            if priors_tau is None:
                raise ValueError("No prior knowledge.")
            _ = self.create_correlated_field(priors_tau)
        flnL_tau, _ = flnL_func(self.tau, flnL0)
        add = ift.Adder(ift.full(flnL_tau.target, lnL_max))
        lnL_tau = add @ ((-1)*((-1)*flnL_tau).exp())
        l_op = lnL_tau.exp().ducktape_left(self.l_key)
        self.l_op = l_op
        self._update_avail_ops(self.l_key, self.l_op)
        return l_op

    def flnL_func(tau, flnL0):
        """ Generate reparametrized log-likelihood prior volume function operator from correlated field operator

                    Parameters:
                        tau (class:'ift.Operator'): Correlated field operator

                    Returns:
                        flnL_func (class:'ift.Operator'): Reparametrized log-likelihood prior volume function operator
                        flnL_func_deriv (class:'ift.Operator'): Derivative of flnL_func
        """
        flnL_func_deriv = ((-tau).exp()) 
        itg = Integrator(flnL_func_deriv.target)
        add = ift.Adder(ift.full(itg.target, flnL0))
        flnL_func = add @ itg @ flnL_func_deriv
        return flnL_func, flnL_func_deriv