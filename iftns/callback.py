# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# Copyright(C) 2013-2021 Max-Planck-Society

import matplotlib.pyplot as plt
import numpy as np
import os

from .sugar_plot import plot_rec_prior_volumes, plot_evidence_histogram
from .ns_functions import get_lnZ, calculate_analytic_evidence
def calculate_evidence_list_from_prior_volume_sample_list(sl, samples, lnX_info):

    """ Calculate the evidence given the reconstructed prior volumes and the data on the likelihood
    dead contours from nested sampling

                Parameters:
                        sl (class:'ift.SampleList'): Reconstructed posterior samples
                        samples (class:'anesthetic.samples.NestedSamples'): Nested sampling run

                Return:
                        lnZ_list (list): List of calculated evidences

    """
    lnL_data = samples.logL.to_numpy()
    sample_list = [np.reshape(sample[lnX_info.lnX_key].val, lnL_data.shape) for
                   sample in sl.iterator(lnX_info.lnX_op)]
    lnZ_list = [get_lnZ(-np.reshape(sample, lnL_data.shape), lnL_data) for sample in sample_list]
    return lnZ_list

def calculate_evidence_list_from_ns_samples(samples, n_samples):

    """ Calculate the evidence given the nested sampling run for the statistical and
    the deterministic approach

                Parameters:
                        samples (class:'anesthetic.samples.NestedSamples'): Nested sampling run
                        n_samples (int): Number of samples taking for the statistical approach


                Return:
                        lnZ_list (list): List of calculated evidences (statistical)
                        lnZ_det (float): Calculated mean (deterministic) evidence

    """

    lnL_data = samples.logL.to_numpy()
    lnX_samples = samples.logX(n_samples).to_numpy()
    lnX_det = samples.logX(nsamples=None).to_numpy()
    lnZ_list = []
    for sample in range(lnX_samples.shape[1]):
        lnZ_list.append(get_lnZ(lnX_samples[:, sample], lnL_data))
    lnZ_det = get_lnZ(lnX_det, lnL_data)
    return lnZ_list, lnZ_det

def save_evidence_info_to_file(filename, actual_evidence=None,
                               ns_evidence_list=None,
                               ns_det_evidence_mean=None,
                               ift_evidence_list= None):

    """ Save the calculated evidence values to a .txt-file

                Parameters:
                        filename (class:'anesthetic.samples.NestedSamples'): Nested sampling run
                        actual_evidence (float): Analytically calculated evidence
                        ns_evidence_list (list): List of NS evidences (statistical)
                        ns_det_evidence_mean (float): Mean (deterministic) NS evidence
                        ift_evidence_list (list): List of IFT inferred posterior sample evidences

    """

    with open(filename, 'w') as f:
        if actual_evidence is not None:
            f.write(f'actual evidence: {actual_evidence}\n')
        if ns_evidence_list is not None:
            f.write(f'ns stat. evidence: {np.mean(ns_evidence_list)}, '
                    f'std= {np.std(ns_evidence_list)}\n')
        if ns_det_evidence_mean is not None:
            f.write(f'ns det evidence: {ns_det_evidence_mean}\n')
        if ift_evidence_list is not None:
            f.write(f'ift evidence: {np.mean(ift_evidence_list)}, '
                    f'std= {np.std(ift_evidence_list)}\n')

def plot_prior_volumes_rec_results_from_sample_list(sl, lnX_info, ns_samples, setup=None,
                                                        gt=None, gt_grid=None, output_dir=None):

    """ Wrapper function to plot the reconstructed prior volumesand histogram of calculated
     evidences alongide with the pure NS results If desired it is possible to also ülot the griund truth.

                Parameters:
                        sl (class:'ift.SampleList'): Reconstructed posterior samples of log prior volumes
                        lnX_info (class:'ins.lnXPrior'): Class for prior volume reconstruction prior
                        ns_samples (class:'anesthetic.samples.NestedSamples'): Nested sampling run
                        setup (dict): Dictionary with likelihood data information
                        setup (dict): Dictionary with likelihood data information
                        gt (np.array): Ground truth array for the log prior volumes
                        gt_grid (np.array): X-axis grid for ground truth plotting
                        output_dir (str): Folder to which to save the images. If None, images are only shown and not saved

    """
    # Plot rec of prior volumes into upper plot
    lnL_data = ns_samples.logL.to_numpy()
    sample_list = [(-1)*np.reshape(sample[lnX_info.lnX_key].val, lnL_data.shape) for
                   sample in sl.iterator(lnX_info.lnX_op)]
    lnX_ns_det = ns_samples.logX(nsamples=None).to_numpy()
    lnX_ns_stat = ns_samples.logX(len(sample_list)).to_numpy()
    lnX_ns_stat_list = [lnX_ns_stat[:, i] for i in
                        range(lnX_ns_stat.shape[1])]


    prior_volumes = [sample_list, lnX_ns_stat_list, lnX_ns_det]
    colors = ['green', 'gray', 'black']
    labels = ['posterior', 'NS stat', 'NS det']
    fig, axs = plt.subplots(2, 1, figsize=(10, 8))

    plot_rec_prior_volumes(axs[0], prior_volumes, r'$\ln X$',
                           lnL_data, r'$\ln L$', colors, labels)

    if gt is not None and gt_grid is not None:
        axs[0].plot(gt_grid, gt, color='red', label='gt')

    # Calculate evidences
    ift_lnZ = calculate_evidence_list_from_prior_volume_sample_list(sl, ns_samples, lnX_info)
    ns_lnZ_stat, ns_lnZ_det = calculate_evidence_list_from_ns_samples(ns_samples, len(sample_list))
    if setup:
        gt_lnZ = calculate_analytic_evidence(setup)
    else:
        gt_lnZ = None

    # Save veidence info
    if output_dir is not None:
        save_evidence_info_to_file(os.path.join(output_dir, 'lnZ_info.txt'),
                                   actual_evidence=gt_lnZ,
                                   ns_evidence_list=ns_lnZ_stat,
                                   ns_det_evidence_mean=ns_lnZ_det,
                                   ift_evidence_list= ift_lnZ)

    # Plot evidence histogram
    evidences = [ift_lnZ, ns_lnZ_stat, ns_lnZ_det, gt_lnZ]
    colors = ['green', 'gray', 'black', 'red']
    labels = ['posterior', 'NS stat', 'NS det', 'gt']

    plot_evidence_histogram(axs[1], evidences, colors, labels, legend=False)
    plt.subplots_adjust(hspace=0.5)
    if output_dir:
        plt.savefig(os.path.join(output_dir, 'prior_vol.png'))
    else:
        plt.show()
    plt.close()


