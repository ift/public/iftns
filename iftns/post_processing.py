# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# Copyright(C) 2013-2021 Max-Planck-Society

import numpy as np
import os

import nifty8 as ift
from .utils import  (combine_list_of_sample_lists, yaml_to_dict,
                     check_dicts_similartity, save_dict_to_yaml)
from .callback import plot_prior_volumes_rec_results_from_sample_list
from .sugar_plot import plot_rec_func_from_sample_list

def check_rec_info_folders(comb_folder):
    """ Helper function to check that folders, which are in the path comb_folder and should be combined,
    have the same subfolders.

            Parameters:
                    comb_folder (str): Path to folder, which contains all reconstruction folders,
                                       that should be combined
    """

    rec_info_sets = []
    for subfolder in os.listdir(comb_folder):
        subfolder_path = os.path.join(comb_folder, subfolder)
        if os.path.isdir(subfolder_path):
            rec_info = sorted([d for d in os.listdir(subfolder_path)
                                          if (d.startswith('rec_info')
                                              and not d.endswith('_0'))])
            rec_info_sets.append(set(rec_info))

    if not all(s == rec_info_sets[0] for s in rec_info_sets):
        raise ValueError("Folders can not be combined, as they do not have"
                         "the same reconstruction folder.")

def check_configs(comb_folder):
    """ Helper function to check that folders, which are in the path comb_folder and should be combined,
    have configs which only differ int the random seed and directory in which the results are saved.

            Parameters:
                    comb_folder (str): Path to folder, which contains all reconstruction folders,
                                       that should be combined
    """

    config_list = []
    for subfolder in os.listdir(comb_folder):
        subfolder_path = os.path.join(comb_folder, subfolder)
        config_path = os.path.join(subfolder_path, 'rec_config.yaml')
        cfg = yaml_to_dict(config_path)
        config_list.append(cfg)
    check_dicts_similartity(config_list)


def create_combined_folder(comb_folder):
    """
    Helper function that generates in the path comb_folder a new driectory called "combined", which
    containts all the reconstruction folder, from the reconstruction folders that should be combined
    and the corresponding config.

            Parameters:
                    comb_folder (str): Path to folder, which contains all reconstruction folders,
                                       that should be combined
    """

    subfolder = os.path.join(comb_folder, os.listdir(comb_folder)[0])
    rec_infos = [d for d in os.listdir(subfolder)
                            if (d.startswith('rec_info')
                                and not d.endswith('_0'))]
    config_path = os.path.join(subfolder, 'rec_config.yaml')
    cfg = yaml_to_dict(config_path)
    combined_folder = os.path.join(comb_folder, 'combined')
    os.makedirs(combined_folder, exist_ok=True)
    save_dict_to_yaml(cfg, os.path.join(combined_folder,
                                         'rec_config.yaml'))
    for rec_info in rec_infos:
        combined_rec_info = os.path.join(combined_folder, rec_info)
        os.makedirs(combined_rec_info, exist_ok=True)

    return combined_folder

def combine_rec_info_sample_lists(comb_folder):
    """
    Wrapper function that iterates through all folders contained in comb_folder and combines
    the contained sample lists, saving the result to a folder in comb_folder called "combined".

            Parameters:
                    comb_folder (str): Path to folder, which contains all reconstruction folders,
                                       that should be combined
    """
    combined_folder = os.path.join(comb_folder, 'combined/')
    rec_infos = [d for d in os.listdir(combined_folder)
                           if (d.startswith('rec_info')
                               and not d.endswith('_0'))]
    for rec_info in rec_infos:
        combined_pickle_folder = os.path.join(combined_folder, rec_info, 'pickle')
        os.makedirs(combined_pickle_folder, exist_ok=True)

        sl_list = []
        sl_mean_list = []
        for subfolder in os.listdir(comb_folder):
            if subfolder != 'combined':
                subfolder_path = os.path.join(comb_folder, subfolder)
                pickle_path = os.path.join(subfolder_path, rec_info, 'pickle')
                sl_base = os.path.join(pickle_path, 'last')
                sl_list.append(ift.ResidualSampleList.load(sl_base))
        sl = combine_list_of_sample_lists(sl_list)
        sl.save(os.path.join(combined_pickle_folder, 'last'))


def plot_comb_sl_results(comb_folder, func_info, lnX_info, ns_samples,
                         x_grid, x_label, data_config,
                         data_dict, gt_dict):
    """
    Wrapper function that plots the sample list information (reconstructed prior volumes and
    likelihood prior volume function) contained in the folder comb_folder/combined, which contains
    the combination of several sample lists.

            Parameters:
                    comb_folder (str): Path to folder, which contains all reconstruction folders,
                                       that should be combined
                    func_info (class:'ins.FuncPrior'): Class for likelihood-prior-volume function prior
                    lnX_info (class:'ins.lnXPrior'): Class for prior volume reconstruction prior
                    ns_samples (class:'anesthetic.samples.NestedSamples'): Nested sampling run
                    x_grid (np.array): Grid for the x-axis
                    x_label (str): Label describing the x-axis
                    data_config (dict): Dictionary with likelihood data information
                    data_dict (dict): Dictionary containing likelihood and prior volume data
                    gt_dict (dict): Dictionary containing the ground truth for each operator
    """
    combined_folder = os.path.join(comb_folder, 'combined/')
    rec_infos = [d for d in os.listdir(combined_folder)
                           if (d.startswith('rec_info')
                               and not d.endswith('_0'))]
    for rec_info in rec_infos:
        os.makedirs(os.path.join(combined_folder, rec_info, 'figs'))
        combined_pickle_folder = os.path.join(combined_folder, rec_info, 'pickle')
        sl_base = os.path.join(combined_pickle_folder, 'last')
        sl = ift.ResidualSampleList.load(sl_base)
        plot_rec_func_from_sample_list(sl, func_info, x_grid, x_label,
                                           data_dict, gt_dict,
                                           output_dir=os.path.join(combined_folder,
                                                                   rec_info, 'figs'))
        plot_prior_volumes_rec_results_from_sample_list(sl, lnX_info,
                                                            ns_samples,
                                                            data_config,
                                                            np.log(gt_dict[func_info.l_key]),
                                                            x_grid,
                                                            os.path.join(combined_folder,
                                                                         rec_info,
                                                                         'figs'))






