# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# Copyright(C) 2013-2021 Max-Planck-Society

import os
import yaml

import nifty8 as ift
def yaml_to_dict(path_to_yaml_file):

    """
    Loads .yaml to dictionary

            Parameters:
                    path_to_yaml_file (str): Dictionary for likelihood setup
            Returns:
                    dict (dict): a dictionary containing all the information stored in the .yaml file
    """

    with open(path_to_yaml_file, "r") as yaml_file:
        dct = yaml.safe_load(yaml_file)
    return dct

def save_dict_to_yaml(dict, yaml_path):
    """
    Save config file to given file path.

            Parameters:
                    dict (dict): Arbitrary dictionary
                    yaml_path (str): Path to aml file, which will be saved to
    """

    with open(yaml_path, "w") as f:
        yaml.dump(dict, f)

def combine_list_of_sample_lists(sls):
    '''
    Combine a list of sample lists into a single sample list.

    This function takes a list of sample lists and combines them into a single sample list. The function aggregates the
    residuals and negative log-likelihoods from each sample list and calculates the mean of the means. The function
    returns a new sample list with the combined residuals and negative log-likelihoods.

            Parameters: 
                    sls (list): A list of sample lists to be combined.

    '''
    residuals = []
    negs = []
    sc = ift.StatCalculator()
    for samples in sls:
        residuals.extend(list(samples._r))
        negs.extend(list(samples._n))
        sc.add(samples._m)
    return ift.ResidualSampleList(sc.mean, residuals, negs)


def check_dicts_similartity(dicts):
    '''
    Check if dictionaries are similar.

    This function takes a list of dictionaries and checks if they are similar. The function checks if the dictionaries
    have the same keys, if they have the same values for the same keys, if they have the same output directories, and
    if they have the same seeds. If any of these conditions are not met, the function raises a ValueError.

            Parameters:
                    dicts (list): A list of dictionaries to be checked for similarity.
    '''

    keys = set(dicts[0].keys())
    output_dirs = set()
    seeds = set()

    ref_dict = {k: v for k, v in dicts[0].items() if k not in {'results_dir', 'seed'}}

    for d in dicts:
        if set(d.keys()) != keys:
            raise ValueError('Dictionaries have different keys')
        if d['results_dir'] in output_dirs:
            raise ValueError('Duplicate output directory in dictionary')
        if d['seed'] in seeds:
            raise ValueError(f'Duplicate seed {d["seed"]} in dictionary')
        output_dirs.add(d['results_dir'])
        seeds.add(d['seed'])

        for k in ref_dict.keys():
            if d[k] != ref_dict[k]:
                raise ValueError(f'For key {k} the values in the dict differ')

def update_config(cfg, update, key, output_new,
                  output_old=None, master=True):
    '''
    Update configuration file.

    This function updates a configuration file with new values. The function takes a configuration file, a dictionary
    containing the new values, a key to update, and the path to the new configuration file. The function updates the
    configuration file with the new values and saves the updated configuration file to the specified path. If the
    master flag is set to True, the function also saves the old configuration file to a specified path.

            Parameters:
                    cfg (dict): The configuration file to be updated.
                    update (dict): The new values to be added to the configuration file.
                    key (str): The key to be updated in the configuration file.
                    output_new (str): The path to the new configuration file.
                    output_old (str): The path to the old configuration file.
                    master (bool): A flag wether the MPI master process is updating the configuration file.
    '''
    if master and output_old is not None:
        save_dict_to_yaml(cfg, output_old)
    cfg[key] = update
    if master:
        if os.path.exists(output_new):
            os.remove(output_new)
        save_dict_to_yaml(cfg, output_new)


