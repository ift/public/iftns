# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# Copyright(C) 2013-2021 Max-Planck-Society

import math
import numpy as np
from scipy.special import logsumexp

import nifty8 as ift

from anesthetic.examples.utils import random_ellipsoid
from anesthetic import NestedSamples
from anesthetic.samples import merge_nested_samples

from .integrate import Integrator, Summation

def gaussian_sum(nlive, ndims, a, m1, m2, sigma1=0.1, sigma2=0.1, R=1, logLmin=-1e-2):
    """Perfect nested sampling run for a the sum of to spherical Gaussians & prior using
    anesthetic (https://arxiv.org/abs/1905.04768)

                Parameters:
                    nlive (int): Number of live points
                    ndims (int): Dimensionality of gaussian
                    a (float): Relative weight of Gaussians
                    m1 (float): Mean of first Gaussian weighted by a
                    m2 (float): Mean of second Gaussian weighted by (1-a)
                    sigma1 (float): Std of first Gaussian weighted by a
                    sigma2 (float): Std of second Gaussian weighted by (1-a)
                    R (float): Radius of Gaussian prior
                    logLmin (float): Loglikelihhod at which to terminate

                Returns:
                    samples (class:'anesthetic.samples.NestedSamples'): Nested sampling run
    """

    def logLike1(x):
        return -((x-m1)**2).sum(axis=-1)/2/sigma1**2

    def logLike2(x):
        return -((x-m2)**2).sum(axis=-1)/2/sigma2**2

    def random_sphere(n):
        return random_ellipsoid(np.zeros(ndims), np.eye(ndims), n)

    samples = []
    r = R
    logL_birth = np.ones(nlive) * -np.inf
    logL = logL_birth.copy()
    while logL.min() < logLmin:
        points = r * random_sphere(nlive)
        logL = np.log(a * np.exp(logLike1(points)) + (1-a) * np.exp(logLike2(points)))
        samples.append(NestedSamples(points, logL=logL, logL_birth=logL_birth))
        logL_birth = logL.copy()
        r = (points**2).sum(axis=-1, keepdims=True)**0.5

    samples = merge_nested_samples(samples)
    logLend = samples.loc[samples.nlive >= nlive].logL.max()
    return samples.loc[samples.logL_birth < logLend].recompute()

def lndX(lnX):
    """ Utitlity function calculating
    

                Parameters:
                        lnX(np.array):
                Returns:
                        lndX (np.array):
    """
    lnXp = np.concatenate(([0], lnX[:-1]))
    lnXm = np.concatenate((lnX[1:], [-np.inf]))
    lndX = np.log(1 - np.exp(lnXm - lnXp)) + lnXp - np.log(2)
    return lndX


def get_lnZ(lnX, lnL):

    """
    Calculate the log of the partition function Z.

    This function computes the log of the partition function Z, denoted as lnZ, 
    using the log prior volumes ans log likelihood contours as inputs. The calculation is based on the weighted sum 
    of lnX and lnL, where the weight function is defined internally.

            Parameters:
                lnX (float): The natural logarithm of prior volumes X
                lnL (float): The natural logarithm of likelihood contours L.

            Returns:
                float: The natural logarithm of the partition function Z, calculated as 
                       the log-sum-exp of the weighted sum of lnX and lnL.

            Note:
                This function requires the definition of `lndX` and `logsumexp` functions 
                which are not provided within this snippet. Ensure these functions are 
                correctly defined and imported in your workspace for proper operation.
    """

    def lnw(lnX, lnL):
        return lndX(lnX) + lnL

    return logsumexp(lnw(lnX, lnL))

def get_lnL_max(x_data, lnL_data, dim):
    """ Estimate maximum log-likelihood according to https://arxiv.org/abs/1908.09139
                Parameters:
                    x_data (np.array): Array of prior volumes
                    lnL_data (np.array): Array of log-likehoods
                    dim (int): Number of dimensions

                Returns:
                    lnL_max (float): Calculated maximum log-likelihood
    """
    lnZ = get_lnZ(np.log(x_data), lnL_data)
    return np.sum(np.exp(lndX(np.log(x_data)))*
                  np.exp(lnL_data)*lnL_data/np.exp(lnZ)) + dim/2


def reparametrize_lnL(lnL, lnL_max):
    """ Reparametrize the log-likelihood via f(lnL) = -ln(max(lnL)-lnL)

                Parameters:
                    lnL (np.array): Array of log-likehoods
                    lnL_max (float): Maximum log-likelihood

                Returns:
                    flnL (np.array): Array of reparametrized log-likelihoods
    """
    flnL = -np.log(lnL_max-lnL)
    return flnL


def create_neglnX_from_contraction_factors(t):
    """ Create negative log-prior-volumes given the contraction factors

                Parameters:
                    t (class:'ift.Operator'): Contraction factor operator

                Returns:
                    minus_lnX (class:'ift.Operator'): Negative log-prior-volume operator
    """
    oned_domain = ift.DomainTuple.make(ift.UnstructuredDomain(t.target.shape[1]))
    reshaper = ift.DomainChangerAndReshaper(t.target, oned_domain)
    sum = Summation(oned_domain)
    lnX = sum(reshaper((-1)*t.log()))
    transform = reshaper.adjoint
    minus_lnX = transform(lnX)
    return minus_lnX


def flnL_func(tau, flnL0):
    """ Generate reparametrized log-likelihood prior volume function operator from correlated field operator

                Parameters:
                    tau (class:'ift.Operator'): Correlated field operator

                Returns:
                    flnL_func (class:'ift.Operator'): Reparametrized log-likelihood prior volume function operator
                    flnL_func_deriv (class:'ift.Operator'): Derivative of flnL_func
    """
    flnL_func_deriv = (1) * ((-tau).exp()).clip(0, 10)
    itg = Integrator(flnL_func_deriv.target)
    add = ift.Adder(ift.full(itg.target, flnL0))
    flnL_func = add @ itg @ flnL_func_deriv
    return flnL_func, flnL_func_deriv

def get_flnL0(setup, flnL_data=None):
    """ Calculate the initial value of the reparametrized log-likelihood-prior-volume function either
        analytically or given the data

                 Parameters:
                        setup (dict): Dictionary with likelihood data information
                        flnL_data (np.array): Data for the reparametrized log-likelihood contours
                 Returns:
                        flnL0 (float): Initial value for reparametrized log-likelihood-prior-volume function
     """
    shape = setup['lklhd_shape']
    if shape == 'anesthetic_gauss':
        return np.log(2*setup['sigma_1']**2)
    if shape == 'anesthetic_summed_gauss':
        return -np.log(-np.log(setup['a']*np.exp(-1/(2*setup['sigma_1']**2))+
                             (1-setup['a'])*np.exp(-1/(2*setup['sigma_2']**2))))
    else:
        if flnL_data is None:
            ift.logger.error('a0 can not be calculated analytically but no data was given.')
        return flnL_data[0]

def get_ground_truth_contours(setup, grid, func_info, flnL0, lnL_max):
    """ Calculate the ground truth likelihood contours given the information n the likelihood. If
        the analytic ground truth is not available None is returned.

                Parameters:
                        setup (dict): Dictionary with likelihood data information
                        grid (np.array): Grid, on which the ground truth should be calculated
                        func_info (class:'ins.FuncPrior'): Class for log-likelihood-prior volume function
                        flnL0 (float): Initial value for the log-likelihood-prior-volume-function
                        lnL_max (float): Maximum log-likelihood

                Returns:
                        gt_dict (dict): Dictionary of ground truths for the reparametrized log-likelihood-prior-volume
                                        function and the likelihood-prior-volume-function
    """
    shape = setup['lklhd_shape']
    if shape == 'anesthetic_gauss':
        flnL_gt = flnL0 + 2 / setup['dim'] * grid
        L_gt = np.exp(lnL_max -  np.exp((-1) * flnL_gt))
    elif shape == 'anesthetic_summed_gauss':
        L_gt = (setup['a']*np.exp(-np.exp(-2/setup['dim']*grid)/(2*setup['sigma_1']**2))+
                (1-setup['a'])*np.exp(-np.exp(-2/setup['dim']*grid)/(2*setup['sigma_2']**2)))
        flnL_gt = -np.log(lnL_max-np.log(L_gt))
    else:
        flnL_gt = None
        L_gt = None
    return {func_info.flnL_key: flnL_gt, func_info.l_key: L_gt}

def get_offset_mean(setup, flnL_data=None, x_data=None, flnL_gt=None, x_gt=None):
    """ Calculate the offset mean of the correlated field
        using either the ground truth or the data
                Parameters:
                        setup (dict): Dictionary with likelihood data information
                        flnL_data (np.array): Data on reparametrized log-likelihood contours
                        x_data (np.array): Data on prior volumes
                        flnL_data (np.array): Ground truth for reparametrized log-likelihood contours
                        x_gt (np.array): Ground truth prior volumes
                Returns:
                        offset_mean (float): Value for offset mean of the correlated field
    """

    def get_log_absolute_slope(flnL_ref, x_ref):
        return -np.log(-(flnL_ref[-1] - flnL_ref[0]) /
                       (np.log(x_ref[-1]) - np.log(x_ref[0])))

    shape = setup['lklhd_shape']
    if shape == 'anesthetic_gauss':
        offset_mean = - np.log(2/setup['dim'])
        return offset_mean
    if shape == 'anesthetic_summed_gauss':
        if flnL_gt is None or x_gt is None:
            raise NotImplementedError
        else:
            offset_mean = get_log_absolute_slope(flnL_gt, x_gt)
        return offset_mean
    else:
        if flnL_data is None or x_data is None:
            raise NotImplementedError
        else:
            offset_mean = get_log_absolute_slope(flnL_data, x_data)
            return offset_mean

def calculate_analytic_evidence(setup):
    """ Calculate the ground truth evidence, if analytically available

                Parameters:
                        setup (dict): Dictionary with likelihood data information
                Returns:
                        gt: Calculated analytic evidence
    """
    if setup['lklhd_shape'] == 'anesthetic_gauss':
        return np.log(math.factorial(int(setup['dim'] / 2)) *
                                 (2 * setup['sigma_1'] ** 2) ** (setup['dim'] / 2))
    elif setup['lklhd_shape'] == 'anesthetic_summed_gauss':
        return np.log(math.factorial(int(setup['dim'] / 2)) *
                      ((setup['a'] * (2 * setup['sigma_1'] ** 2) ** (
                                         setup['dim'] / 2)) +
                                 (1 - setup['a']) * (2 * setup['sigma_2'] ** 2) ** (
                                         setup['dim'] / 2)))
    else:
        return None