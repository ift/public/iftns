# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# Copyright(C) 2013-2021 Max-Planck-Society

import numpy as np

import nifty8 as ift
class Integrator(ift.LinearOperator):
    """ Integration Operator for a one-dimensional field

            Parameters
                    dom (class:'ift.Domain'): Domain of the field, which will be integrated
    """
    def __init__(self, dom):
        self._domain = ift.makeDomain(dom)
        self._target = ift.makeDomain(dom)
        self._capability = self.TIMES | self.ADJOINT_TIMES
        if not len(dom.shape) == 1:
            raise NotImplementedError('only for 1d')

    def apply(self, x, mode):
        self._check_input(x, mode)
        v = x.val
        if mode == self.TIMES:
            r = np.cumsum(v)*self._domain.scalar_weight()
            return ift.Field.from_raw(self._target, r)
        else:
            r = np.flip(np.cumsum(np.flip(v))) * self._domain.scalar_weight()
            return ift.Field.from_raw(self._domain, r)

class Summation(ift.LinearOperator):
    """ Summation Operator for a one-dimensional field

            Parameters
                    dom (class:'ift.Domain'): Domain of the field, which will be summed over
    """
    def __init__(self, dom):
        self._domain = ift.makeDomain(dom)
        self._target = ift.makeDomain(dom)
        self._capability = self.TIMES | self.ADJOINT_TIMES
        if not len(dom.shape) == 1:
            raise NotImplementedError('only for 1d')

    def apply(self, x, mode):
        self._check_input(x, mode)
        v = x.val
        if mode == self.TIMES:
            r = np.cumsum(v)
            return ift.Field.from_raw(self._target, r)
        else:
            r = np.flip(np.cumsum(np.flip(v)))
            return ift.Field.from_raw(self._domain, r)