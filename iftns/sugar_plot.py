# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# Copyright(C) 2013-2021 Max-Planck-Society

import matplotlib.pyplot as plt
import numpy as np
import os

import nifty8 as ift

def plot_ns_prior_samples(flnL_info, grid, grid_label,
                          gt_dict = {}, y_labels={}, n_samples=10, output_dir= None):
    """ Plot prior samples for available operators in flnL_info

                Parameters:
                    flnL_info (class:'ins.FuncPrior'): Class for likelihood-prior-volume function prior
                    grid (np.array): Grid for the x-axis
                    grid_label (str): Label describing the x-axis
                    gt_dict (dict): Dictionary containing the ground truth for each operator, if wanted
                    y_labels (dict): Dictionary containing the label for the y-axis for each operator
                    n_samples (int): Number of samples plotted for each operator
                    output_dir (str): Folder to which to save the images. If None, images are only shown and not saved

    """
    # Create n_samples latent space samples and apply available operators
    sample_dict = {key: [] for key in flnL_info.avail_ops.keys()}
    for i in range(n_samples):
        mock = ift.from_random(flnL_info.tau.domain)
        for key in flnL_info.avail_ops.keys():
            sample = flnL_info.padder.adjoint(flnL_info.avail_ops[key](mock)[key])
            sample_dict[key].append(sample)
    # Plot sample lists per operator
    for key in sample_dict.keys():
        for i in range(len(sample_dict[key])):
            if i == 0:
                plt.plot(grid, sample_dict[key][i].val, label='prior samples', color='gray')
            else:
                plt.plot(grid, sample_dict[key][i].val, color='gray')
        if key in gt_dict and gt_dict[key] is not None:
            plt.plot(grid, gt_dict[key], label='ground truth', color='red')
        plt.xlabel(grid_label)
        if key in y_labels:
            plt.ylabel(y_labels[key])
        else:
            plt.ylabel(key)
        plt.legend()
        plt.title('Prior samples')
        if output_dir is not None:
            plt.savefig(os.path.join(output_dir, f'{key}_prior_samples.png'))
        else:
            plt.show()
        plt.close()

def plot_rec_func_from_sample_list(sl, flnL_info, grid, grid_label,
                                   data_dict = {}, gt_dict=None,
                                   y_labels= {}, output_dir=None, fs=14):
    """ Plot reconstructed likelihood prior volume function on the given grid given the sample list

                Parameters:
                    sl (class:'ift.SampleList'): Reconstructed posterior samples
                    flnL_info (class:'ins.FuncPrior'): Class for likelihood-prior-volume function prior
                    grid (np.array): Grid for the x-axis
                    grid_label (str): Label describing the x-axis
                    data_dict (dict): Dictionary containing the data from nested sampling, if wanted
                    gt_dict (dict): Dictionary containing the ground truth for each operator, if wanted
                    y_labels (dict): Dictionary containing the label for the y-axis for each operator
                    output_dir (str): Folder to which to save the images. If None, images are only shown and not saved

    """
    for key in flnL_info.avail_ops.keys():
        mean, unc = sl.sample_stat(flnL_info.avail_ops[key])
        plt.plot(grid, flnL_info.padder.adjoint(mean[key]).val, color='green',
                 label='rec function')
        plt.fill_between(grid,
                         flnL_info.padder.adjoint(mean[key]).val - np.sqrt(flnL_info.padder.adjoint(unc[key]).val),
                         flnL_info.padder.adjoint(mean[key]).val + np.sqrt(flnL_info.padder.adjoint(unc[key]).val),
                         color='lightgreen')
        if key and 'x' in data_dict and data_dict[key] is not None and data_dict['x'] is not None:
            plt.plot(np.log(data_dict['x']), data_dict[key], color='black', label='data')
        if gt_dict is not None and gt_dict[key] is not None:
            plt.plot(grid, gt_dict[key], '--', color='red', label='ground truth')
        plt.legend(shadow=False, frameon=False, fontsize=fs - 2)
        plt.xlabel(grid_label, fontsize=fs)
        if key in y_labels:
            plt.ylabel(y_labels[key], fontsize=fs)
        else:
            plt.ylabel(key, fontsize=fs)
        if output_dir:
            plt.savefig(os.path.join(output_dir, f'{key}_rec.png'))
        else:
            plt.show()
        plt.close()

def plot_evidence_histogram(ax, evidences, colors, labels, fs=10, legend=True):

    """ Plot the histograms of evidence lists listed in evidences. If the entries are not lists
    but floats, a vertical line is drawn.

                Parameters:
                        evidences (list): List of evidence list  of samples or one calculated evidence float
                        colors (str): List of colors for each entry in evidences
                        labels (str): List of labels for each entry in evidences
                        fs (int): Fontsize on figure
                        legend (bool): True if legend shall be plotted



                Return:
                        lnZ_list (list): List of calculated evidences (statistical)
                        lnZ_det (float): Calculated mean (deterministic) evidence

    """

    for i, entry in enumerate(evidences):
        if isinstance(entry, list):
            ax.hist(entry, alpha=0.5, label=labels[i], color=colors[i])
            plt.axvline(x=np.mean(entry), color=colors[i],
                        label=f'{labels[i]} mean = {round(np.mean(entry), 1)} '
                              r'$\pm$'+'{round(np.std(entry), 1)}')
        elif isinstance(entry, float):
            ax.axvline(x=entry, label=labels[i], color=colors[i])
        else:
            ift.logger.info(f'Entry {labels[i]} of type '
                            f'{type(entry).__name__}could not be plotted')

    ax.set_xlabel(r'$\ln Z$')
    ax.set_ylabel('Frequency')
    if legend:
        ax.legend(shadow=False, frameon=False, fontsize=fs - 2)

def plot_rec_prior_volumes(ax, prior_volumes, x_axis_label, y_axis,
                           y_axis_label, colors, labels, fs=10, legend=True):

    """ Plot the reconstruction of the prior volumes
    listed in prior_volumes. If the entries are lists, a set of samples for this
    entry is plotted.

                Parameters:
                        evidences (list): List of evidence list  of samples or one calculated evidence float
                        colors (str): List of colors for each entry in evidences
                        labels (str): List of labels for each entry in evidences
                        fs (int): Fontsize on figure
                        legend (bool): True if legend shall be plotted



                Return:
                        lnZ_list (list): List of calculated evidences (statistical)
                        lnZ_det (float): Calculated mean (deterministic) evidence

    """
    for i, entry in enumerate(prior_volumes):
        if isinstance(entry, list):
            for j, sample in enumerate(entry):
                if j == 0:
                    ax.plot(sample, y_axis,
                                color=colors[i], alpha=0.5, linewidth=0.5,
                                label=labels[i])
                else:
                    ax.plot(sample, y_axis, color=colors[i], alpha=0.5,
                                linewidth=0.5)
            ax.plot(np.mean(np.array(entry), axis=0), y_axis, colors[i],
                    label=f'{labels[i]} mean')
        elif isinstance(entry, np.ndarray):
            ax.plot(entry, y_axis, color=colors[i], label=labels[i])
        else:
            ift.logger.info(f'Entry {labels[i]} of type '
                            f'{type(entry).__name__} could not be plotted')
    ax.set_xlabel(x_axis_label)
    ax.set_ylabel(y_axis_label)
    if legend:
        ax.legend(shadow=False, frameon=False, fontsize=fs - 2)








