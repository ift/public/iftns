# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# Copyright(C) 2013-2021 Max-Planck-Society

import nifty8 as ift
import numpy as np

def _compute_FD_gradients(points, field):
    """
    Compute finite difference gradients of a field at specified points.

    This function calculates the gradients of a given field at specified points using finite differences. The points
    are given in a coordinate system aligned with the field's domain. The function supports multi-dimensional fields
    and calculates the gradient for each dimension.

            Parameters:
                    points (np.ndarray): An array of shape (N, M) where N is the number of dimensions and M is the number of points.
                    The array specifies the coordinates at which the gradients are to be computed.
                    field (ift.Field): The field for which gradients are computed. The field must have a domain attribute that
                    specifies the distances between adjacent points in each dimension.

            Returns:
                    np.ndarray: An array of shape (N, M) containing the computed gradients for each point and dimension. The first
                    dimension corresponds to the different axes of the input field, and the second dimension corresponds to the
                    different points.
    """
    shp = points.shape
    f = field.val
    ndim = shp[0]
    dist = list(field.domain[0].distances)
    dist = np.array(dist).reshape(-1,1)
    pos = points/dist
    excess = pos - np.floor(pos)
    pos = np.floor(pos).astype(np.int64)
    max_index = np.array(field.domain.shape).reshape(-1,1)
    data = np.zeros(shp)
    mg = np.mgrid[(slice(0,2),)*ndim]
    mg = np.array(list(map(np.ravel, mg)))
    
    for axis in range(ndim):
        dx = field.domain[0].distances[axis]
        for i in range(len(mg[0])):
            factor = np.abs(1 - mg[:, i].reshape(-1, 1) - excess)
            factor[axis,:] = (mg[axis,i]*2-1) / dx
            indx = (pos+mg[:,i].reshape(-1,1))
            indx[0,:] = indx[0,:] % field.shape[0]
            val = np.prod(factor, axis=0)*f[tuple(indx)]
            data[axis, :] += val
    return data


class PartialVdotOperator(ift.LinearOperator): 
    """
    A linear operator that performs a partial vector dot product.

    It takes a field as input and performs a dot product along the first axis of the field with another field,
    effectively summing over the rows multiplied by the field elements. It supports both forward and
    adjoint operations.

            Parameters:
                mat (ift.Field): The field with which the dot product will be performed.
    """
    def __init__(self, mat):    
        self._domain = mat.domain
        shp = mat.domain.shape
        self._target = ift.makeDomain(ift.UnstructuredDomain(shp[1]))
        self._mat = mat.val
        self._capability = self.TIMES | self.ADJOINT_TIMES

    def apply(self, x, mode):
        """
        Apply the partial dot product operation to a field x.

        This method supports both forward (TIMES) and adjoint (ADJOINT_TIMES) operations. In the forward mode, it
        computes the sum of mat rows multiplied by x. In the adjoint mode, it multiplies mat
        by x without summing over the rows.

                Parameters:
                        x (ift.Field): The input vector field.
                        mode (int): The operation mode (TIMES or ADJOINT_TIMES).

                Returns:
                        ift.Field: The result of the partial dot product operation.
        """
        self._check_input(x, mode)
        v = x.val
        if mode == self.TIMES:
            val = np.sum(self._mat*v, axis=0)
            return ift.Field.from_raw(self._target, val)
        else:
            val = self._mat*v
            return ift.Field.from_raw(self._domain, val)



class Interpolation(ift.Operator):
    """
    Multilinear interpolation for variable points in an RGSpace

            Parameters
                    rg_dom (class:'ift.Domain'): RGSpace domain
                    rg_key  (str): key of RGSpace in multifield dictionary
                    points_dom (class:'ift.Domain'): domain of interpolation points (unstructured)
                    point_key (str): key of points domain in multifield dictionary
    """
    def __init__(self, rg_dom, rg_key, point_dom, point_key):
        if not isinstance(rg_dom, ift.RGSpace):
            raise TypeError
        if not isinstance(point_dom, ift.UnstructuredDomain):
            raise TypeError
        shp = point_dom.shape
        if len(shp) != 2:
            raise ValueError('Point domain shape length incompatible')
        if shp[0] != len(rg_dom.shape):
            raise ValueError('Point domain incompatible with RGSpace')
        self._domain = ift.MultiDomain.make({rg_key:rg_dom,
            point_key:point_dom})
        self._target = ift.makeDomain(ift.UnstructuredDomain(shp[1]))
        self._rg_key = rg_key
        self._point_key = point_key

    def apply(self, x):
        """
        Apply the interpolation operation to the input data x.

        This method performs multilinear interpolation on the input data based on the RGSpace and points domains
        specified during the initialization of the Interpolation object. It supports both direct values and linearizations
        as input and applies the interpolation accordingly.

                Parameters:
                        x (ift.MultiField): The input data containing fields for both the RGSpace and the points domain. The data
                        should be structured according to the keys specified during initialization (`rg_key` for RGSpace and
                        `point_key` for points domain).

                Returns:
                        ift.Field or ift.Linearization: The result of the interpolation. The type of the output matches the type of the
                        input (i.e., if the input is a Linearization, the output will also be a Linearization).

    """
        self._check_input(x)
        lin = isinstance(x, ift.Linearization)
        xval = x
        if lin:
            xval = x.val
        ps = xval[self._point_key]
        gd = xval[self._rg_key]
        ps = ps.val
        gd = gd.val
        LI = ift.LinearInterpolator(self._domain[self._rg_key], ps)
        if not lin:
            return LI(x[self._rg_key])
        val = LI(xval[self._rg_key])
        d1 = LI.ducktape(self._rg_key)
        dat = _compute_FD_gradients(ps, xval[self._rg_key])
        grad = ift.Field.from_raw(x.target[self._point_key], dat)
        d2 = PartialVdotOperator(grad).ducktape(self._point_key)
        return x.new(val, d1+d2)


