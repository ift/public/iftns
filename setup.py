from setuptools import setup, find_packages

setup(
    name='iftns',
    version='1.0.0',
    author='Margret Westerkamp, Jakob Roth',
    author_email='margret@mpa-garching.mpg.de, roth@mpa-garching.mpg.de',
    packages=find_packages(include=['iftns', 'iftns.*']),  # Explicitly specify packages
    include_package_data=True,
    long_description=open('README.md').read(),
    long_description_content_type='text/markdown',
    install_requires=[
        'numpy',
        'jax',
        'jaxlib',
        'pyyaml'
    ],
)